package service;

import dao.IdentifiableDao;
import entities.Booking;
import entities.Person;
import io.Console;
import io.UnixConsole;

import java.util.*;

public class BookingService {
    Console console = new UnixConsole(new Scanner(System.in));
    IdentifiableDao<Booking> bookingDao = new IdentifiableDao<>("booking.bin");

    public void book(int num, int raceID) {
        List<Person> people = new ArrayList<>();
        for (int i = 1; i <= num; i++) {
            console.printLn("Enter the name of the person #" + i);
            String name = console.readLn();
            console.printLn("Enter the surname of the person #" + i);
            String surname = console.readLn();
            Person person = new Person(name, surname);
            people.add(person);
        }
        Booking booking = new Booking(people, raceID);
        bookingDao.create(booking);
    }

    public void cancel(int id) {
        bookingDao.delete(id);
    }

    public Collection<Booking> getBooks() {
        return bookingDao.getAll();
    }

    public Collection<Booking> getAll(){
        return bookingDao.getAll();
    }
}