package service;

import dao.RaceDao;
import entities.Race;

import java.util.*;
import java.util.stream.Collectors;

public class RaceService {
    RaceDao raceDao = new RaceDao("races.txt");

    public Collection<String> showAll() {
        Collection<Race> all = raceDao.getAll();
        return all.stream()
                .map(r -> "" + r)
                .collect(Collectors.toList());
    }

    public Optional<String> getOne(int id) {
        Optional<Race> specific = raceDao.get(id);
        return specific.map(Race::one);
    }

    public Collection<Race> getMine(String dest, String date, int num) {
        return raceDao.getAllBy(r -> r.getDestination().equals(dest) && r.getDate().equals(date) && r.getCapacity() >= num);

    }

}
