package service;

import entities.Booking;
import entities.Person;
import entities.Race;
import io.Console;
import io.UnixConsole;

import java.util.*;

public class Service {
    Console console = new UnixConsole(new Scanner(System.in));
    RaceService raceService = new RaceService();
    BookingService bookingService = new BookingService();

    public Collection<String> showAll() {
        return raceService.showAll();
    }

    public Optional<String> getOne() {
        console.print("Enter the ID of the particular race\n");
        int i = 0;
        try {
            i = Integer.parseInt(console.readLn());
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        return raceService.getOne(i);
    }

    public void getMine() {
        console.print("Enter the destination\n");
        String dest = console.readLn();
        console.print("Enter the date\n");
        String date = console.readLn();
        console.print("Enter the number of tickets\n");
        int num = 0;
        try {
            num = Integer.parseInt(console.readLn());
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        Collection<Race> mine = raceService.getMine(dest, date, num);
        mine.forEach(System.out::println);
        console.print("Enter the ID of the particular race or 0 (to exit)\n");
        int idToFind = Integer.parseInt(console.readLn());
        if (idToFind != 0) {
            bookingService.book(num, idToFind);
        }
    }

    public String cancel() {
        console.print("Enter the ID of the race you want to cancel\n");
        int id = 0;
        try {
            id = Integer.parseInt(console.readLn());
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        bookingService.cancel(id);
        return String.format("Element under %d id was permanently deleted", id);
    }

    public String getBooks() {
        StringBuilder s = new StringBuilder();
        Collection<Booking> books = bookingService.getBooks();
        books.forEach(b -> {
            s.append(b);
            s.append("\n");
        });
        return s.toString();
    }

    public List<Optional<String>> getAll() {
        Collection<Booking> all = bookingService.getAll();
        console.printLn("Enter the name of the person");
        String name = console.readLn();
        console.printLn("Enter the surname of the person");
        String surname = console.readLn();
        List<Integer> ids = new ArrayList<>();
        for (Booking booking : all) {
            for (Person person : booking.getPeople()) {
                if (person.getName().equals(name) && person.getSurname().equals(surname)) {
                    ids.add(booking.getRaceID());
                }
            }
        }
        List<Optional<String>> hisRaces = new ArrayList<>();
        for (int id : ids) {
            hisRaces.add(raceService.getOne(id));
        }
        return hisRaces;
    }
}
