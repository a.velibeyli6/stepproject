package controller;

import service.Service;

import java.util.*;

public class Controller {
    Service service = new Service();

    public void showAll() {
        Collection<String> all = service.showAll();
        all.forEach(System.out::println);
    }

    public void showOne() {
        Optional<String> one = service.getOne();
        System.out.println(one);
    }

    public void showMine() {
        service.getMine();
    }

    public void cancel() {
        System.out.println(service.cancel());
    }

    public void getAll(){
        List<Optional<String>> all = service.getAll();

        all.forEach(System.out::println);

    }

    public void getBooks() {
        String books = service.getBooks();
        System.out.println(books);
    }

}
