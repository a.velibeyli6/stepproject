package entities;

import java.io.*;

public class Person implements Serializable {
    public final String name;
    public final String surname;

    private static final long serialVersionUID = 1L;

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        return String.format("Person{name='%s', surname='%s'}", name, surname);
    }
}
