package entities;

import java.io.Serializable;

public class Race {
    private String destination;
    private String date;
    private int capacity;
    private int time;
    public final int id;

    public Race(String destination, String date, int capacity, int time, int id) {
        this.destination = destination;
        this.date = date;
        this.capacity = capacity;
        this.time = time;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getDestination() {
        return destination;
    }

    public String getDate() {
        return date;
    }

    public int getCapacity() {
        return capacity;
    }

    public static Race parse(String r) {
        String[] splited = r.split(":");
        return new Race(
                splited[0],
                splited[1],
                Integer.parseInt(splited[2]),
                Integer.parseInt(splited[3]),
                Integer.parseInt(splited[4])
        );
    }

    public String represent() {
        return String.format("%s:%s:%d:%d:%d", destination, date, capacity, time, id);
    }

    @Override
    public String toString() {
        return String.format("date = %-10s\tdestination = %-12s\tcapacity = %-6d\tid = %d", date, destination, capacity, id);
    }

    public String one() {
        return String.format("destination = %s\tdate = %s\tcapacity = %d\ttime = %d\tid = %d", destination, date, capacity, time, id);
    }
}
