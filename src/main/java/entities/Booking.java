package entities;



import dao.Identifiable;
import dao.IdentifiableDao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class Booking implements Serializable, Identifiable {
    private List<Person> people;
    private int raceID;
    public final long id;
    private static long ids;

    private static final long serialVersionUID = 1L;

    {
        Collection<Identifiable> all = new IdentifiableDao<>("booking.bin").getAll();
        ids = all.stream().map(Identifiable::getId).max((id1, id2) -> (int) (id1 - id2)).orElse((long) 0);
    }

    public Booking(List<Person> person, int raceID) {
        this.people = person;
        this.raceID = raceID;
        this.id = ++ids;
    }

    public long getId() {
        return id;
    }

    public List<Person> getPeople() {
        return people;
    }

    public int getRaceID() {
        return raceID;
    }

    @Override
    public String toString() {
        return String.format("Booking{person=%s, raceID=%d, id=%d}", people.toString(), raceID, id);
    }
}
