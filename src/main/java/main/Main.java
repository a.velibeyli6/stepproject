package main;

import controller.Controller;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {

        Controller controller = new Controller();
        StringBuilder print = new StringBuilder();
        print.append("+--------------------------------------------------------+\n");
        print.append("|        TICKET RESERVATION SYSTEM (BARCLAYS AIRWAYS)    |\n");
        print.append("+--------------------------------------------------------+\n");
        print.append("| 1. TO SEE ALL RACES AVAILABLE WITHIN 24 HOURS          |\n");
        print.append("| 2. TO SEE ALL THE INFORMATION ABOUT PARTICULAR ONE     |\n");
        print.append("| 3. TO BOOK ONE                                         |\n");
        print.append("| 4. TO CANCEL ONE                                       |\n");
        print.append("| 5. TO SEE ONE'S LIST OF RACES                          |\n");
        print.append("| 6. TO EXIT                                             |\n");
//        print.append("| 7. Bookings                                            |\n");
        print.append("+--------------------------------------------------------+\n");
        print.append("|SELECT MENU (1-6) ->                                    |\n");
        print.append("+--------------------------------------------------------+\n");
        Scanner in = new Scanner(System.in);
        int option = 0;
        System.out.println(print.toString());
        try {
            option = Integer.parseInt(in.nextLine());
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }
        while (option != 6) {
            switch (option) {
                case 1:
                    controller.showAll();
                    break;
                case 2:
                    controller.showOne();
                    break;
                case 3:
                    controller.showMine();
                    break;
                case 4:
                    controller.cancel();
                    break;
                case 5:
                    controller.getAll();
                    break;
//                case 7:
//                    controller.getBooks();
            }
            System.out.println(print.toString());
            try {
                option = Integer.parseInt(in.nextLine());
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }
        }

    }
}
