package dao;

import entities.Race;

import java.io.*;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class RaceDao implements DAO<Race> {
    public final String filename;

    public RaceDao(String filename) {
        this.filename = filename;
    }

    @Override
    public Optional<Race> get(int id) {
        return getAll().stream()
                .filter(r -> r.id == id)
                .findFirst();
    }

    @Override
    public Collection<Race> getAll() {
        try {
            return new BufferedReader(new FileReader(new File(filename)))
                    .lines()
                    .map(Race::parse)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException("Something went wrong");
        }
    }

    @Override
    public Collection<Race> getAllBy(Predicate<Race> p) {
        return getAll().stream().filter(p) // r -> p.test(r) OR  p::test
                .collect(Collectors.toList());
    }

    @Override
    public void create(Race newItem) {
        Collection<Race> all = getAll();
        all.add(newItem);
        write(all);
    }

    @Override
    public void delete(int id) {
        Collection<Race> allBy = getAllBy(r -> r.getId() != id);
        write(allBy);
    }

    private void write(Collection<Race> races) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))) {
            for (Race race: races) {
                bw.write(race.represent());
                bw.write("\n");
            }
        } catch (IOException ex) {
            throw new RuntimeException("DAO:write:IOException", ex);
        }
    }
}
